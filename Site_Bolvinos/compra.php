<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Menu do Usuario </title>
		<link rel="stylesheet" href="_css/cabecalhoPrivadoPosInicial.css">
		<link rel="stylesheet" href="_css/compra.css">
		
		<script>
			function conversorAK(){
				var x = document.getElementById("a").value;
				document.getElementById("k").value = x*15;
			}
			
			function conversorKA(){
				var x = document.getElementById("k").value;
				document.getElementById("a").value = x/15;
			}
		</script>
		
	</header>
	
	
	<body>
		
		<div id="cabecalho"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<ul type = "default" ><!-- start = "default"> para ol // Foto de eberhard grossgasteiger no Pexels-->
						<li> <a href ="areaPrivada.php"> Tela Inicial </a> </li>
						
						<li> <a href ="compra.php"> Compra </a> </li> 
					
						<li> <a href ="venda.php"> Venda </a> </li>
						
						<li> <a href ="pesagem.php"> Pesagem </a> </li>
						
						<li>  Gastos 
							<ul id="sub">
								<li id="prima"> <a href ="gastoIndividual.php"> Individual </a> </li>
								<li> <a href ="gastoRebanho.php"> Rebanho </a> </li>
							</ul>
						</li>
						
						<li> <a href ="nascidos.php"> Nascidos </a> </li>
						
						<li> <a href ="mortes.php"> Mortes </a> </li>
						
						<li> <a href ="relatorios.php"> Relatorios </a> </li>
						
						
					</ul>
				</nav>
				
				<img id="icone" src = "_imagens/logo2.png"/>
				
			</header>
		</div>
		
		
		<img id="inicio" src = "_imagens/ap9.png" width="1333"/>
		
<!--============================================================================================================================-->
		
		
		<div id="desenvolvimento"> 
		
			<form method="POST" action="classes/cadCompra.php">
				<h1> Cadastar Compra </h1>
				<section>
					<p> Nº de Identificação: <input type="text" placeholder="Numero de Identificação" name="numeroIdentificacao" maxlength="10"/> </p>
					
					<p>Sexo: 
						<select name="sexo">
							<option value="M" > Macho </option>
							<option value="F" > Femea </option>
						</select> 
					</p>
					
					<p> Raça:
						<select name="raca">
							<option value="Desconhecido"> Desconhecido </option>
							<option value="Girolando"> Girolando </option>
							<option value="Gir Leiteiro"> Gir Leiteiro </option>
							<option value="Holandês"> Holandês </option>
							<option value="Holandês"> Jersey </option>
							<option value="Nelore"> Nelore </option>
							<option value="Zebu"> Zebu </option>
							<option value="Outros"> Outros </option>
						</select> 
					</p>
				
				</section>
				
				<section>
					<p> Peso em Arroba @: <input type="text" placeholder="Peso em @" name="pesoA" maxlength="5" oninput="conversorAK()" id="a"/> </p>
					
					<p> Peso em Quilo kg: <input type="text" placeholder="Peso em @" name="quilo" maxlength="7" oninput="conversorKA()" id="k"/> </p>
					
					<p> Valor da Arroba @: <input type="text" placeholder="Valor Da @" name="valorA" maxlength="6"/> </p>
				
				</section>
				
				<section>
					<p id="naofloat"> Data da Compra<input type="date" name="data" /> </p>
				</section>
				
				<input type="submit" value="Cadastrar" id="cadastrar" name="cadastrar">
				
			</form>
			
		</div>
		
		
	</body>
	
	
</html>