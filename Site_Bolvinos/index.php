
<!DOCTYPE html>

<html lang = "pt-br">

	<head>
		<meta charset = "UTF-8"/>
		<title> @Bolvinos </title>
		<link rel = "stylesheet" href = "_css/t_inicio.css"/>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
	</head>
<!--==================================================================-->

	<body>
		
		<header id = "cabecalho">
			<nav id = "menu">
				<ul type = "default" ><!-- start = "default"> pra ol-->
					<li> <a href ="	index.php"> Página Inicial </a> </li>
					
					<li> <a href ="login.php"> Login </a> </li>
					
					<li> <a href ="faleConosco.php"> Fale conosco </a> </li>
				</ul>
			</nav>
			
			<img id="icone" src = "_imagens/logo2.png"/>
			
		</header>
		
	
<!--=======================================================================================================================================
Foto de Brett Sayles no Pexels-ftoanimas tras da arvore--> 
	
		<div> <!--=== principal ===-->
			
			<img id="inicio" src = "_imagens/t2.png" width="1333"/>
			
			<h1 id="recepcao"> SEJAM BEM VINDO(AS)! </h1>
			
			
			<div id="desenvolvimento"> 
			
				<div id="noticiario">
					<h1 id="ultimasNoticias"> Últimas Notícias! </h1>
					
					<section id="noticia1"> 
						<h2> Malu - Disco Arranhado </h2>
						
						<p> Quebra a rotina</p> 
						<p> E leva sua mina pra dançar </p>
						<p> Ao som daquela banda cover </p>
						<br/>
						<p> Vaaaaai </p>
						<p> Compra bebida de litro </p>
						<p> E chapa com ela </p>
						<p> Faz essa noite </p>
						<p> A melhor da vida dela </p>
						
					</section> 
					
					
					
					<aside id="noticia2">
						<h2> Banda Cover - Zé Neto e Cristiano </h2>
						
						<p> Vem ser a minha semana </p>
						<p> Meu fim de semana e o meu feríado </p>
						<p> Meu remédio controlado </p>
						<p> O meu disco arranhado </p>
						<p> Naquela parte que diz eu te amo, te amo </p>
						<p> Te amo, te amo, te amo, te amo, te amo, te amo. </p>
					</aside>
					
					
				</div>
				
				
				<h1 id="fraseMotivacional"> O amor é a sabedoria dos loucos e a loucura dos sábios. - Samuel Johnson </h1>
				
			
			</div>
		 
			
		</div>
		
		
<!--=======================================================================================================================================-->
		
		
		<div id="rodape">
			
			<p id="campos">
				<a href="termosDeServicos.php" id="i"> Termos de Serviço </a> //
					
				<a href="referenciasBibliográfica.php" id="i"> Referencias Bibliográfica </a>
			</p>
				
			<footer>
				
				<p> Copyright &copy; 2020 - by João Ferreira <br/>
				<a href="https://www.facebook.com/profile.php?id=100013593917417" target="_blank" id="facebook"> Facebook</a> |
				
				<a href="https://www.instagram.com/joao.ferreira9899" target="_blank" id="instagram"> Instagram </a>
				</p>
				
			</footer>
			
		</div>
		
<!--=======================================================================================================================================-->

	</body>


</html>



