<?php
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_fazenda']) || !isset($_SESSION['id_rebanho']) ){
		header("location: login.php");
		exit;
	}
	
	include_once("classes/conexao.php");
/*--------------------------------------------------*/

	$numeroAnimais;

/*===================================================================================================================================*/

	function buscardados($IntervaloDeTempo){
		
		$dataInicio = date('Y-m-d', strtotime('-'.$IntervaloDeTempo.' days', strtotime(date('Y-m-d'))));
		
		$dataCliente = date('d/m/Y', strtotime($dataInicio));
		
		echo "<p> Serão Contabilizadas Todas as Compras que Foram Realizadas </p> <p id='a'> Entre ".$dataCliente." e ".date('d/m/Y')."</p>";
		contarAnimais($dataInicio);
	}
	
/*===================================================================================================================================*/

	function contarAnimais($dataInicio){
		global $conn;
		global $numeroAnimais;
		
		$sql = "select count(*) FROM saidaVenda where id_rebanho = ".$_SESSION['id_rebanho']." and dataSaida >= '".$dataInicio."';";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			$numeroAnimais = mysqli_fetch_array($resultado);
			return true;
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01');
				window.history.back();
			</script>";
			return false;
		}
	}
	
/*===================================================================================================================================*/
?>


<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Menu do Usuario </title>
		<link rel="stylesheet" href="_css/r_Compra.css">
		
		<script>
			function imprimir(){
				var elem = document.getElementById("imprimir");
				
				elem.style.visibility = "hidden"; 
				
				window.print();
				
				elem.style.visibility = "visible";
			
			}
		</script>
		
	</header>
	
	
	<body>
		<h1> Relatorio de Venda </h1>
		
			<fieldset id='maior'> <legend> &nbsp; Vendas Realizadas na Ultima Semana &nbsp;</legend>
				<?php
				
					buscardados(7);
					global $numeroAnimais;
					
					echo"<table id='tabelaspec'>
					
						<tr> <td class='ce'> Foram Vendidos: </td> <td class='cd'> ".$numeroAnimais[0]." Animais </td></tr>
						
						<tr> <td class='ce'> Tendo Um Valor Estimado Em: </td> <td class='cd'> R$ reais</td> </tr>
						
						<tr> <td class='ce'> Com um Lucro Estimado Em: </td> <td class='cd'> R$ reais</td> </tr>
						
					</table>";
				?>
			</fieldset>
			
		<button onclick="imprimir()" id="imprimir"> Imprimir Relatorio </button>
	
	</body>
	
</html>