<?php

	session_start();
	if(!isset($_SESSION['id_fazenda']) || !isset($_SESSION['id_rebanho']) || !isset($_SESSION['id_login'])){
		header("location: login.php");
		exit;
	}
	
	include_once("conexao.php");
	
/*----------------------------------------------------------*/
		/*----- Parametros Passados pela URL -----*/
		
	$nIdentif = addslashes($_POST['nIdent']);
	$valorGasto = addslashes($_POST['valorGasto']);
	$causagasto = addslashes($_POST['causagasto']);
	$data = addslashes($_POST['data']);
	
	$cadastrar = addslashes($_POST['cadastrar']);
	
/*----------------------------------------------------------*/
	$idAnimal;
	
/*=======================================================================================*/

	if(isset($cadastrar)) {
		if( camposPreenchidos($nIdentif, $valorGasto, $causagasto, $data) ){
			if(valoresNumericos($valorGasto)){
				if(formatarCamposNumericos()){
					
					if(buscarIdAnimal($nIdentif)){
						cadgasto($valorGasto, $data, $causagasto);
					}
					
				}
			}
		}
		
	}
	
/*=======================================================================================*/	

	function camposPreenchidos($nIdentif, $valorGasto, $causagasto, $data){ 
	
		if( ($nIdentif =="" || $nIdentif == null) || ($valorGasto == "" || $valorGasto == null) || 
			($causagasto == "" || $causagasto == null) || ($data == "" || $data = null)){
			echo "<script >
					alert('Todos os Campos Devem Ser Preenchidos!');
					window.history.back();
				</script>";
			return false;
		}
		return true;
	}
	
/*=======================================================================================*/	
	
	function valoresNumericos($valorGasto){
		if(!is_numeric ($valorGasto)){
			echo "<script >
					alert('Campo Numerico Preennchido de Forma Incorreta!');
					window.history.back();
				</script>";
			return false;
		}
		return true;
	}
	
/*=======================================================================================*/	

	function formatarCamposNumericos(){
		global $valorGasto;
		$valorGasto = number_format($valorGasto, 2, '.', '');
		
		return true;
	}

/*=======================================================================================*/	

	function buscarIdAnimal($nIdentif){
		global $conn;
		global $idAnimal;
		
		$sql = "SELECT id_animal FROM animal WHERE id_rebanho = ".$_SESSION['id_rebanho']." and numeroDeIdentificacao = ".$nIdentif.";";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			if( ($dadosAnimal = mysqli_fetch_array($resultado)) == null){
				echo "<script language='javascript' type='text/javascript'>
					alert('Animal ãao Encontrado Em Nossos Cadastros \\n Verifique a Regularidade Dos Dados Fornecidos!');
					window.history.back();
				</script>";
				return false;
			}
			$idAnimal = $dadosAnimal[0];
			return true;
			
		}else{
			echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\n Pedimos Desculpas Pelo Transtorno! 01');
					window.history.back();
				</script>";
			return false;
		}
	}
	
/*=======================================================================================*/	

	function cadgasto($valorGasto, $data, $causagasto){
		global $conn;
		global $idAnimal;
		
		$sql = "INSERT INTO gastoIndividual VALUES (default, ".$_SESSION['id_rebanho'].", ".$idAnimal.",".$valorGasto.", '".$data."', '".$causagasto."');";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			echo "<script language='javascript' type='text/javascript'>
					alert('Cadastrado Realizado com Sucesso!');
					window.location.href='../gastoIndividual.php';
				</script>";
		}else{
			echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 02');
					window.history.back();
				</script>";
		}
	}
/*=======================================================================================*/	



?>



