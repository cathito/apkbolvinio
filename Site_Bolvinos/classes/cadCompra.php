<?php

	session_start();
	if(!isset($_SESSION['id_fazenda']) || !isset($_SESSION['id_rebanho']) || !isset($_SESSION['id_login'])){
		header("location: login.php");
		exit;
	}
	
	include_once("conexao.php");
	
/*----------------------------------------------------------*/
		/*----- Parametros Passados pela URL -----*/
		
	$sexo = addslashes($_POST['sexo']);
	$raca = addslashes($_POST['raca']);
	$pesoA = addslashes($_POST['pesoA']);
	$quilo = addslashes($_POST['quilo']);
	$valorA = addslashes($_POST['valorA']);
	$data = addslashes($_POST['data']);
	$nIdentif = addslashes($_POST['numeroIdentificacao']);
	
	$cadastrar = addslashes($_POST['cadastrar']);
	
/*=======================================================================================*/


	if(isset($cadastrar)) {
		
		if( camposPreenchidos($nIdentif, $sexo, $raca, $pesoA, $quilo, $valorA, $data) ){
			if(valoresNumericos($pesoA, $quilo, $valorA)){
				if(formatarCamposNumericos()){
					
					if(cadastrarAnimal($nIdentif, $sexo, $raca,  $data)){
						cadastrarDadosDaCompra($nIdentif, $pesoA, $quilo, $valorA, $data);
					}
					
				}
			}
		}
		
	}
	
/*=======================================================================================*/	

	function camposPreenchidos($nIdentif, $sexo, $raca, $pesoA, $quilo, $valorA, $data){ 
	
		if( ($nIdentif =="" || $nIdentif == null) || ($sexo == "" || $sexo == null) || ($raca == "" || $raca == null) || 
			($pesoA == "" || $pesoA == null) || ($quilo == "" || $quilo == null) || ($valorA == "" || $valorA == null) ||
			($data == "" || $data == null) ){
			echo "<script >
					alert('Todos os Campos Devem Ser Preenchidos!');
					window.history.back();
				</script>";
			return false;
		}
		return true;
	}
	
/*=======================================================================================*/	
	function valoresNumericos($pesoA, $quilo, $valorA){
		if(!is_numeric ($pesoA) || !is_numeric ($quilo) || !is_numeric ($valorA) ){
			echo "<script >
					alert('Campo Numerico Preennchido de Forma Incorreta!');
					window.history.back();
				</script>";
			return false;
		}
		return true;
	}
	
/*=======================================================================================*/	
	
	function formatarCamposNumericos(){
		global $pesoA;
		global $quilo;
		global $valorA;
		
		$pesoA = number_format($pesoA, 2, '.', '');
		$quilo = number_format($quilo, 2, '.', '');
		$valorA = number_format($valorA, 2, '.', '');
		
		return true;
	}
	
/*=======================================================================================*/	

	function cadastrarAnimal($nIdentif, $sexo, $raca,  $data){
		global $conn;
		
		$sql = "insert into animal values (default, ".$_SESSION['id_rebanho'].", ".$nIdentif.", '".$sexo."', '".$raca."', '".$data."', default, default);";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			return true;
		}else{
			echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01');
					window.history.back();
				</script>";
			return false;
		}
		
	}
/*=======================================================================================*/	
	
	function cadastrarDadosDaCompra($nIdentif, $pesoA, $quilo, $valorA, $data){
		global $conn;
		
		$sql = "SELECT id_animal FROM animal WHERE numeroDeIdentificacao = ".$nIdentif." and id_rebanho =".(int)$_SESSION['id_rebanho'].";";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			if( ($dadosAnimal = mysqli_fetch_array($resultado)) != null){
				$valorAnimal = $pesoA*$valorA;
				$valorAnimal = number_format($valorAnimal, 2, '.', '');
				
				$sql = "insert into dadosCompra values (default, ".$_SESSION['id_rebanho'].", ".$dadosAnimal[0].", ".$pesoA.", ".$quilo.", ".$valorA.", ".$valorAnimal.", '".$data."', default);";
				$resultado = mysqli_query($conn, $sql);
		
				if ($resultado) {
					echo "<script language='javascript' type='text/javascript'>
							alert('Animal Cadastrado Com Sucesso!');
							window.location.href='../compra.php';
						</script>";
				}else{
					echo "<script language='javascript' type='text/javascript'>
							alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 03');
							window.history.back();
						</script>";
				}
				
			}
			else{
				echo "<script language='javascript' type='text/javascript'>
					alert('Animal Não Encontrado Em Nossos Cadastros \\n Verifique a Regularidade Dos Dados Fornecidos!');
					window.history.back();
				</script>";
			}
		}else{
			echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 02');
					window.history.back();
				</script>";
		}
	}
	
/*=======================================================================================*/	

	
?>




