<?php

	session_start();
	if(!isset($_SESSION['id_fazenda']) || !isset($_SESSION['id_rebanho']) || !isset($_SESSION['id_login'])){
		header("location: login.php");
		exit;
	}
	
	include_once("conexao.php");
	
/*----------------------------------------------------------*/
		/*----- Parametros Passados pela URL -----*/
	$nIdentif = addslashes($_POST['nIdent']);
	$pesoA = addslashes($_POST['pesoA']);
	$quilo = addslashes($_POST['quilo']);
	$data = addslashes($_POST['data']);
	
	$cadastrar = addslashes($_POST['cadastrar']);
	
/*----------------------------------------------------------*/
	
	$idAnimal;
	
	
/*=======================================================================================*/

	if(isset($cadastrar)) {
		if( camposPreenchidos($nIdentif, $pesoA, $quilo, $data) ){
			if( buscarIdAnimal($nIdentif)){
				cadastrarPesagem($pesoA, $quilo, $data);
			}
		}
		
	}
	
/*=======================================================================================*/	

	function camposPreenchidos($nIdentif, $pesoA, $quilo, $data){ 
	
		if( ($nIdentif =="" || $nIdentif == null) || ($pesoA == "" || $pesoA == null) || 
			($quilo == "" || $quilo == null) || ($data == "" || $data == null) ){
			echo "<script >
					alert('Todos os Campos Devem Ser Preenchidos!');
					window.history.back();
				</script>";
			return false;
		}
		return true;
	}
	
/*=======================================================================================*/	

	function valoresNumericos($pesoA, $quilo){
		if(!is_numeric ($pesoA) || !is_numeric ($quilo) ){
			echo "<script >
					alert('Campo Numerico Preennchido de Forma Incorreta!');
					window.history.back();
				</script>";
			return false;
		}
		return true;
	}
	
/*=======================================================================================*/	

	function formatarCamposNumericos(){
		global $pesoA;
		global $quilo;
		
		$pesoA = number_format($pesoA, 2, '.', '');
		$quilo = number_format($quilo, 2, '.', '');
		
		return true;
	}

/*=======================================================================================*/	

	function buscarIdAnimal($nIdentif){
		global $conn;
		global $idAnimal;
		
		$sql = "SELECT id_animal FROM animal WHERE id_rebanho = ".$_SESSION['id_rebanho']." and numeroDeIdentificacao = ".$nIdentif.";";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			if( ($dadosAnimal = mysqli_fetch_array($resultado)) == null){
				echo "<script language='javascript' type='text/javascript'>
					alert('Animal Não Encontrado Em Nossos Cadastros \\n Verifique a Regularidade Dos Dados Fornecidos!');
					window.history.back();
				</script>";
			}
			$idAnimal = $dadosAnimal[0];
			return true;
			
		}else{
			echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01');
					window.history.back();
				</script>";
		}
	}

/*=======================================================================================*/	

	function cadastrarPesagem($pesoA, $quilo, $data){
		global $conn;
		global $idAnimal;
		
		$sql = "insert into pesagem values (default, ".$_SESSION['id_rebanho'].", ".$idAnimal.", '".$pesoA."', '".$quilo."', '".$data."');";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			echo "<script language='javascript' type='text/javascript'>
					alert('Pesagem Cadastrada Com Sucesso!');
					window.location.href='../pesagem.php';
				</script>";
		}else{
			echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 02');
					window.history.back();
				</script>";
		}
	}

?>