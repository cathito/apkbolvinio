<?php
	session_start();
	if(!isset($_SESSION['id_fazenda']) || !isset($_SESSION['id_rebanho']) || !isset($_SESSION['id_login'])){
		header("location: login.php");
		exit;
	}
	
	include_once("conexao.php");
	
	
	$sexo = addslashes($_POST['sexo']);
	$raca = addslashes($_POST['raca']);
	$data = addslashes($_POST['data']);
	$nIdentif = addslashes($_POST['numeroIdentificacao']);
	
	$cadastrar = addslashes($_POST['cadastrar']);
	
/*=======================================================================================*/	

	if(isset($cadastrar)) {
		if( camposPreenchidos($nIdentif, $sexo, $raca, $data) ){
			cadastrarAnimal($nIdentif, $sexo, $raca,  $data);
		}
	}
	
/*=======================================================================================*/	

	function camposPreenchidos($nIdentif, $sexo, $raca, $data){ 
	
		if( ($nIdentif =="" || $nIdentif == null) || ($sexo == "" || $sexo == null) || 
			($raca == "" || $raca == null) || ($data == "" || $data == null) ){
			echo "<script >
					alert('Todos os Campos Devem Ser Preenchidos!');
					window.history.back();
				</script>";
			return false;
		}
		return true;
	}
	
/*=======================================================================================*/	

	function cadastrarAnimal($nIdentif, $sexo, $raca,  $data){
		global $conn;
		
		$sql = "insert into animal values (default, ".$_SESSION['id_rebanho'].", ".$nIdentif.", '".$sexo."', '".$raca."', '".$data."', false, default);";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			echo "<script language='javascript' type='text/javascript'>
					alert('Cadastrado Realizado com Sucesso!');
					window.location.href='../nascidos.php';
				</script>";
		}else{
			echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01');
					window.history.back();
				</script>";
		}
		
	}
/*=======================================================================================*/	
	



?>