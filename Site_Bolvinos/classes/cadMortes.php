<?php

	session_start();
	if(!isset($_SESSION['id_fazenda']) || !isset($_SESSION['id_rebanho']) || !isset($_SESSION['id_login'])){
		header("location: login.php");
		exit;
	}
	
	
	include_once("conexao.php");
	
	$nIdentif = addslashes($_POST['nIdent']);
	$causaMorte = addslashes($_POST['causaMorte']);
	$data = addslashes($_POST['data']);
	
	$cadastrar = addslashes($_POST['cadastrar']);
	
	$idAnimal;
/*=======================================================================================*/
	
	if(isset($cadastrar)) {
		if( camposPreenchidos($nIdentif, $causaMorte, $data) ){
			if(buscarIdAnimal($nIdentif)){
				if(baixaAnimal()){
					cadSaidaMorte($data, $causaMorte);
				}
			}
		}
		
	}
	
/*=======================================================================================*/	

	function camposPreenchidos($nIdentif, $causaMorte, $data){ 
	
		if( ($nIdentif =="" || $nIdentif == null) || ($causaMorte == "" || $causaMorte == null) || 
			($data == "" || $data == null) ){
			echo "<script >
					alert('Todos os Campos Devem Ser Preenchidos!');
					window.history.back();
				</script>";
			return false;
		}
		return true;
	}
	
/*=======================================================================================*/

	function buscarIdAnimal($nIdentif){
		global $conn;
		global $idAnimal;
		
		$sql = "SELECT id_animal FROM animal WHERE id_rebanho = ".$_SESSION['id_rebanho']." and numeroDeIdentificacao = ".$nIdentif.";";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			if( ($dadosAnimal = mysqli_fetch_array($resultado)) == null){
				echo "<script language='javascript' type='text/javascript'>
					alert('Animal Não Encontrado Em Nossos Cadastros \\n Verifique a Regularidade Dos Dados Fornecidos!');
					window.history.back();
				</script>";
			}
			$idAnimal = $dadosAnimal[0];
			return true;
			
		}else{
			echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01');
					window.history.back();
				</script>";
		}
	}
	
/*=======================================================================================*/	


	function baixaAnimal(){
		global $conn;
		global $idAnimal;
		
		$sql = "UPDATE animal SET emPosse = false WHERE id_animal = ".$idAnimal.";";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			return true;
		}else{
			echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 02');
					window.history.back();
				</script>";
		}
	}
	
/*=======================================================================================*/	

	function cadSaidaMorte($data, $causaMorte){
		global $conn;
		global $idAnimal;
		
		$sql = "INSERT INTO saidaMorte VALUES (default, ".$_SESSION['id_rebanho'].", ".$idAnimal.", '".$data."', '".$causaMorte."');";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			echo "<script language='javascript' type='text/javascript'>
					alert('Cadastrado Realizado com Sucesso!');
					window.location.href='../venda.php';
				</script>";
		}else{
			echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 03');
					window.history.back();
				</script>";
		}
	}
	
/*=======================================================================================*/	


	
?>