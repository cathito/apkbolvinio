<?php

	session_start();
	if(!isset($_SESSION['id_fazenda']) || !isset($_SESSION['id_rebanho']) || !isset($_SESSION['id_login'])){
		header("location: login.php");
		exit;
	}
	
	include_once("conexao.php");
	
/*----------------------------------------------------------*/
		/*----- Parametros Passados pela URL -----*/
		
	$valorGasto = addslashes($_POST['valorGasto']);
	$data = addslashes($_POST['data']);
	$causagasto = addslashes($_POST['causagasto']);
	
	$cadastrar = addslashes($_POST['cadastrar']);
	
/*=======================================================================================*/

	if(isset($cadastrar)) {
		if( camposPreenchidos($valorGasto, $data, $causagasto) ){
			if(valoresNumericos($valorGasto)){
				if(formatarCamposNumericos()){
					cadgasto($valorGasto, $data, $causagasto);
				}
			}
		}
		
	}
	
/*=======================================================================================*/	

	function camposPreenchidos($valorGasto, $data, $causagasto){ 
	
		if(($valorGasto == "" || $valorGasto == null) || ($data == "" || $data = null) ||
			($causagasto == "" || $causagasto == null) ){
			echo "<script >
					alert('Todos os Campos Devem Ser Preenchidos!');
					window.history.back();
				</script>";
			return false;
		}
		return true;
	}
	
/*=======================================================================================*/	
	function valoresNumericos($valorGasto){
		if(!is_numeric ($valorGasto)){
			echo "<script >
					alert('Campo Numerico Preennchido de Forma Incorreta!');
					window.history.back();
				</script>";
			return false;
		}
		return true;
	}
	
/*=======================================================================================*/	

	function formatarCamposNumericos(){
		global $valorGasto;
		$valorGasto = number_format($valorGasto, 2, '.', '');
		
		return true;
	}

/*=======================================================================================*/	
	
	function cadgasto($valorGasto, $data, $causagasto){
		global $conn;
		
		$sql = "INSERT INTO gastorebanho VALUES (default, ".$_SESSION['id_rebanho'].",".$valorGasto.", '".$data."', '".$causagasto."');";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			echo "<script language='javascript' type='text/javascript'>
					alert('Cadastrado Realizado com Sucesso!');
					window.location.href='../gastoRebanho.php';
				</script>";
		}else{
			echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01');
					window.history.back();
				</script>";
		}
	}
	
	
	


?>