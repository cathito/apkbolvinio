<?php

	session_start();
	if(!isset($_SESSION['id_fazenda']) || !isset($_SESSION['id_rebanho']) || !isset($_SESSION['id_login'])){
		header("location: login.php");
		exit;
	}
	
	
	include_once("conexao.php");
	
/*----------------------------------------------------------*/
		/*----- Parametros Passados pela URL -----*/
		
	$nIdentif = addslashes($_POST['nIdent']);
	$pesoA = addslashes($_POST['pesoA']);
	$quilo = addslashes($_POST['quilo']);
	$valorA = addslashes($_POST['valorA']);
	$data = addslashes($_POST['data']);
	$cadastrar = addslashes($_POST['cadastrar']);
	
/*----------------------------------------------------------*/	
		/*----- Variaves -----*/
		
	$idAnimal;
	
	
	
/*=======================================================================================*/
	
	if(isset($cadastrar)) {
		if( camposPreenchidos($nIdentif, $pesoA, $quilo, $valorA, $data) ){
			if(valoresNumericos($pesoA, $quilo, $valorA)){
				if(formatarCamposNumericos()){
					
					if(buscarIdAnimal($nIdentif)){
						if(baixaAnimal()){
							if(baixaDadosCompra()){
								cadSaidaVenda($pesoA, $quilo, $valorA, $data);
							}
						}
					}
					
				}
			}
		}
		
	}
	
/*=======================================================================================*/	

	function camposPreenchidos($nIdentif, $pesoA, $quilo, $valorA, $data){ 
	
		if( ($nIdentif =="" || $nIdentif == null) || ($pesoA == "" || $pesoA == null) || 
			($quilo == "" || $quilo == null) || ($valorA == "" || $valorA == null) ||
			($data == "" || $data == null) ){
			echo "<script >
					alert('Todos os Campos Devem Ser Preenchidos!');
					window.history.back();
				</script>";
			return false;
		}
		return true;
	}
	
/*=======================================================================================*/	

	function valoresNumericos($pesoA, $quilo, $valorA){
		if(!is_numeric ($pesoA) || !is_numeric ($quilo) || !is_numeric ($valorA) ){
			echo "<script >
					alert('Campo Numerico Preennchido de Forma Incorreta!');
					window.history.back();
				</script>";
			return false;
		}
		return true;
	}
	
/*=======================================================================================*/	

	function formatarCamposNumericos(){
		global $pesoA;
		global $quilo;
		global $valorA;
		
		$pesoA = number_format($pesoA, 2, '.', '');
		$quilo = number_format($quilo, 2, '.', '');
		$valorA = number_format($valorA, 2, '.', '');
		
		return true;
	}

/*=======================================================================================*/	

	function buscarIdAnimal($nIdentif){
		global $conn;
		global $idAnimal;
		
		$sql = "SELECT id_animal FROM animal WHERE id_rebanho = ".$_SESSION['id_rebanho']." and numeroDeIdentificacao = ".$nIdentif.";";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			if( ($dadosAnimal = mysqli_fetch_array($resultado)) == null){
				echo "<script language='javascript' type='text/javascript'>
					alert('Animal Não Encontrado Em Nossos Cadastros \\n Verifique a Regularidade Dos Dados Fornecidos!');
					window.history.back();
				</script>";
				return false;
			}
			$idAnimal = $dadosAnimal[0];
			return true;
			
		}else{
			echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01');
					window.history.back();
				</script>";
			return false;
		}
	}
	
/*=======================================================================================*/	


	function baixaAnimal(){
		global $conn;
		global $idAnimal;
		
		$sql = "UPDATE animal SET emPosse = false WHERE id_animal = ".$idAnimal.";";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			return true;
		}else{
			echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 02');
					window.history.back();
				</script>";
			return false;
		}
	}
	
/*=======================================================================================*/	
	
	
	function baixaDadosCompra(){
		global $conn;
		global $idAnimal;
		
		$sql = "UPDATE dadosCompra SET emPosse = false WHERE id_animal = ".$idAnimal.";";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			return true;
		}else{
			echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 02.1');
					window.history.back();
				</script>";
			return false;
		}
	}
	
/*=======================================================================================*/	

	function cadSaidaVenda($pesoA, $quilo, $valorA, $data){
		global $conn;
		global $idAnimal;
		
		$valorAnimal = $pesoA*$valorA;
		$valorAnimal = number_format($valorAnimal, 2, '.', '');
		
		$sql = "INSERT INTO saidaVenda VALUES (default, ".$_SESSION['id_rebanho'].", ".$idAnimal.", ".$pesoA.", ".$quilo.", ".$valorA.", ".$valorAnimal.", '".$data."');";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			echo "<script language='javascript' type='text/javascript'>
					alert('Cadastrado Realizado com Sucesso!');
					window.location.href='../venda.php';
				</script>";
		}else{
			echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 03');
					window.history.back();
				</script>";
		}
	}
	
/*=======================================================================================*/	
	
?>




