<?php
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_fazenda']) || !isset($_SESSION['id_rebanho']) ){
		header("location: login.php");
		exit;
	}
	
	include_once("classes/conexao.php");
	
/*===================================================================================================================================*/

		$dadosUsuario;
		$dadosFazenda;
		$dadosRebanho;
		
		$numeroAnimais;
		$numeroAnimaisMachos;
		$numeroAnimaisFemeas;
		
		$numeroAnimaisNascidos;
		$numeroAnimaisComprados;
	/*-------------------------------*/
		
		function dadosUsuario(){
			global $conn;
			global $dadosUsuario;
			
			$sql = "SELECT id_login, nome, email FROM usuario WHERE id_login = ".$_SESSION['id_login'].";";
			$resultado = mysqli_query($conn, $sql);
			
			if ($resultado) {
				$dadosUsuario = mysqli_fetch_array($resultado);
				return true;
			}else {
				echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 02');
				</script>";
			}
		}
/*===================================================================================================================================*/
		function dadosFazenda(){
			global $conn;
			global $dadosUsuario;
			global $dadosFazenda;
			$sql = "SELECT * FROM fazenda WHERE id_fazenda = ".(int)$_SESSION['id_fazenda'].";";
			$resultado = mysqli_query($conn, $sql);
			
			if ($resultado) {
				$dadosFazenda = mysqli_fetch_array($resultado);
				return true;
			}else {
				echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 04');
				</script>";
			}
		}
/*===================================================================================================================================*/
		function dadosrebanho(){
			global $conn;
			global $dadosFazenda;
			global $dadosRebanho;
			
			$sql = "SELECT * FROM rebanho WHERE id_fazenda = ".(int)$_SESSION['id_fazenda'].";";
			$resultado = mysqli_query($conn, $sql);
			
			if ($resultado) {
				$dadosRebanho = mysqli_fetch_array($resultado);
				return true;
			}else {
				echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\n Pedimos Desculpas Pelo Transtorno! 04');
				</script>";
			}
		}
/*===================================================================================================================================*/
		function contarAnimais(){
			global $conn;
			global $dadosRebanho;
			global $numeroAnimais;
			
			$sql = "SELECT count(*) FROM animal where id_rebanho = ".(int)$_SESSION['id_rebanho']." and emPosse = true;";
			$resultado = mysqli_query($conn, $sql);
			
			if ($resultado) {
				$numeroAnimais = mysqli_fetch_array($resultado);
			}else {
				echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 04');
				</script>";
			}
		}
/*===================================================================================================================================*/
		function contarAnimaisMachos(){
			global $conn;
			global $dadosRebanho;
			global $numeroAnimaisMachos;
			
			$sql = "select count(*) from animal where sexo='M' and id_rebanho = ".(int)$_SESSION['id_rebanho']." and emPosse = true;";
			$resultado = mysqli_query($conn, $sql);
			
			if ($resultado) {
				$numeroAnimaisMachos = mysqli_fetch_array($resultado);
			}else {
				echo $sql."<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 05');
				</script>";
			}
		}
/*===================================================================================================================================*/
		function contarAnimaisFemeas(){
			global $conn;
			global $dadosRebanho;
			global $numeroAnimaisFemeas;
			
			$sql = "select count(sexo) from animal where sexo='F' and id_rebanho = ".(int)$_SESSION['id_rebanho']." and emPosse = true;";
			$resultado = mysqli_query($conn, $sql);
			
			if ($resultado) {
				$numeroAnimaisFemeas = mysqli_fetch_array($resultado);
			}else {
				echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 06');
				</script>";
			}
		}
		
/*===================================================================================================================================*/

		function contarNascidosFazenda(){
			global $conn;
			global $numeroAnimaisNascidos;
			
			$sql = "select count(*) from animal where id_rebanho = ".$_SESSION['id_rebanho']." and chegadaPorCompra = false and emPosse = true;";
			$resultado = mysqli_query($conn, $sql);
			
			if ($resultado) {
				$numeroAnimaisNascidos = mysqli_fetch_array($resultado);
			}else {
				echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 06');
				</script>";
			}
		}
		
/*===================================================================================================================================*/
		function contaraComprados(){
			global $conn;
			global $numeroAnimaisComprados;
			
			$sql = "select count(*) from animal where id_rebanho = ".$_SESSION['id_rebanho']." and chegadaPorCompra = true and emPosse = true;";
			$resultado = mysqli_query($conn, $sql);
			
			if ($resultado) {
				$numeroAnimaisComprados = mysqli_fetch_array($resultado);
			}else {
				echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 06');
				</script>";
			}
		}
		
/*===================================================================================================================================*/
?>

<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Menu do Usuario </title>
		<link rel="stylesheet" href="_css/relatGeral.css">
		
		<script>
			function imprimir(){
				var elem = document.getElementById("imprimir");
				
				elem.style.visibility = "hidden"; 
				
				window.print();
				
				elem.style.visibility = "visible";
			
			}
		</script>
		
	</header>
	
	
	<body>
		
		<section>
			<h1> Relatorio Geral </h1>
			
			<?php
				$dadosUsuario;
				$dadosFazenda;
				
				if(dadosUsuario()){
					if(dadosFazenda()){
						if(dadosrebanho()){
							contarAnimais();
							contarAnimaisMachos();
							contarAnimaisFemeas();
							contarNascidosFazenda();
							contaraComprados();
						}
					}
				}
/*===================================================================================================================================*/
				
				if($dadosUsuario!=null && $dadosFazenda!=null){
					
					echo "<table id='tabelaspec'>
							<caption> Fazenda $dadosFazenda[2] </caption>
							
							<tr> <td class='ce'> Rebanho Contabilizado Em: </td> <td class='cd'> $numeroAnimais[0] Animais </td></tr>
							
							<tr> <td rowspan='2' class='ce'> Em um Total de: </td> <td class='cd'> $numeroAnimaisFemeas[0] Femeas</td> </tr>
																			  <tr> <td class='cd'> $numeroAnimaisMachos[0] Machos </td> </tr>
							
							
							<tr> <td rowspan='2' class='ce'> Dos quais </td> <td class='cd'> $numeroAnimaisNascidos[0] Nascidos na Fazenda</td> </tr>
																		<tr> <td class='cd'> $numeroAnimaisComprados[0] Comprados </td> </tr>
							
							
						</table>";
				}
/*===================================================================================================================================*/
				
			?>
			
			<button onclick="imprimir()" id="imprimir"> Imprimir Relatorio </button>
			
		</section>
		
		
	</body>
	
</html>