<!DOCTYPE html>

<html lang = "pt-br">

	<head> 
		<meta charset = "UTF-8"/>
		<title> Simula Pre-seed</title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel = "stylesheet" href = "_css/faleConosco.css"/>
	</head>
<!--=======================================================================================================================================-->

	<body>
		
		<header id = "cabecalho">
			<nav id = "menu">
				<ul type = "default" ><!-- start = "default"> para ol-->
					<li> <a href ="	index.php"> Página Inicial </a> </li>
					
					<li> <a href ="login.php"> Login </a> </li>
					
					<li> <a href ="faleConosco.php"> Fale conosco </a> </li>
				</ul>
			</nav>
			
			<img id="icone" src = "_imagens/logo2.png"/>
			
		</header>
		
<!--=======================================================================================================================================-->

		<section id="corpo-full">
			
			<form method="post" id="fContato" action="classes/faleConosco.php"> 
			
				<fieldset id="mensagem"> <legend>&nbsp; Mensagem do Usuário &nbsp;</legend> 
					
					<p> <label > Nome: </label> 
						<input type="text" name="nome" size="58" maxlength="50" placeholder="Nome Completo"/> 
					</p> 
					
					<p> <label> Email: </label> 
						<input type="text" name="email" size="58" maxlength="50" placeholder="Email..."/> 
					</p> 
					<p> <label> Assunto: </label> 
						<input type="text" name="assunto" size="56" maxlength="50" placeholder="Assunto..."/> </p> 
					<p> 
						<textarea name="mensagem" cols="60" rows="8" placeholder="Deixe Aqui Sua Mensagem..."></textarea> 
					</p>
					
				</fieldset>
				
				<input type="submit" value="Enviar" name="enviar" id="enviar">
				
			</form>
			
		</section>
		
<!--=======================================================================================================================================-->
		
		<div id="rodape">
			
			<p id="campos">
				<a href="termosDeServicos.php" id="i"> Termos de Serviço </a> //
					
				<a href="referenciasBibliográfica.php" id="i"> Referencias Bibliográfica </a>
			</p>
				
			<footer>
				
				<p> Copyright &copy; 2020 - by João Ferreira <br/>
				<a href="https://www.facebook.com/profile.php?id=100013593917417" target="_blank" id="facebook"> Facebook</a> |
				
				<a href="https://www.instagram.com/joao.ferreira9899" target="_blank" id="instagram"> Instagram </a>
				</p>
				
			</footer>
			
		</div>
		
<!--=======================================================================================================================================-->
		
		
	</body>


</html>