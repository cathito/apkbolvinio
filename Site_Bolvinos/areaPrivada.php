<?php
	
	session_start();
	if(!isset($_SESSION['id_login'])){
		header("location: login.php");
		exit;
	}
	
	include_once("classes/conexao.php");
	
/*===================================================================================================================================*/

		$dadosUsuario;
		$dadosFazenda;
		$dadosRebanho;
		$numeroAnimais;
		$numeroAnimaisMachos;
		$numeroAnimaisFemeas;
	/*-------------------------------*/
		
		function dadosUsuario(){
			global $conn;
			global $dadosUsuario;
			
			$sql = "SELECT id_login, nome, email FROM usuario WHERE id_login = ".$_SESSION['id_login'].";";
			$resultado = mysqli_query($conn, $sql);
			
			if ($resultado) {
				$dadosUsuario = mysqli_fetch_array($resultado);
				return true;
			}else {
				echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 02');
				</script>";
			}
		}
/*===================================================================================================================================*/
		function dadosFazenda(){
			global $conn;
			global $dadosUsuario;
			global $dadosFazenda;
			
			$sql = "SELECT * FROM fazenda WHERE id_login = ".$dadosUsuario[0].";";
			$resultado = mysqli_query($conn, $sql);
			
			if ($resultado) {
				$dadosFazenda = mysqli_fetch_array($resultado);
				$_SESSION['id_fazenda']=$dadosFazenda;
				
				return true;
			}else {
				echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 04');
				</script>";
			}
		}
/*===================================================================================================================================*/
		function dadosrebanho(){
			global $conn;
			global $dadosFazenda;
			global $dadosRebanho;
			
			$sql = "SELECT * FROM rebanho WHERE id_fazenda = ".$dadosFazenda[0].";";
			$resultado = mysqli_query($conn, $sql);
			
			if ($resultado) {
				$dadosRebanho = mysqli_fetch_array($resultado);
				$_SESSION['id_rebanho']=$dadosRebanho[0];
				return true;
			}else {
				echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\n Pedimos Desculpas Pelo Transtorno! 04');
				</script>";
			}
		}
/*===================================================================================================================================*/
		function contarAnimais(){
			global $conn;
			global $dadosRebanho;
			global $numeroAnimais;
			
			$sql = "SELECT count(*) FROM animal where id_rebanho = ".$dadosRebanho[0]." and emPosse = true;";
			$resultado = mysqli_query($conn, $sql);
			
			if ($resultado) {
				$numeroAnimais = mysqli_fetch_array($resultado);
			}else {
				echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 04');
				</script>";
			}
		}
/*===================================================================================================================================*/
		function contarAnimaisMachos(){
			global $conn;
			global $dadosRebanho;
			global $numeroAnimaisMachos;
			
			$sql = "select count(*) from animal where sexo='M' and id_rebanho = ".$dadosRebanho[0]." and emPosse = true;";
			$resultado = mysqli_query($conn, $sql);
			
			if ($resultado) {
				$numeroAnimaisMachos = mysqli_fetch_array($resultado);
			}else {
				echo $sql."<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 05');
				</script>";
			}
		}
/*===================================================================================================================================*/
		function contarAnimaisFemeas(){
			global $conn;
			global $dadosRebanho;
			global $numeroAnimaisFemeas;
			
			$sql = "select count(sexo) from animal where sexo='F' and id_rebanho = ".$dadosRebanho[0]." and emPosse = true;";
			$resultado = mysqli_query($conn, $sql);
			
			if ($resultado) {
				$numeroAnimaisFemeas = mysqli_fetch_array($resultado);
			}else {
				echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 06');
				</script>";
			}
		}
/*===================================================================================================================================*/
?>


<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Menu do Usuario </title>
		<link rel="stylesheet" href="_css/areaPrivada.css">
		
	</header>
	
	
	<body>
		
		<div id="cabecalho"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<ul type = "default" ><!-- start = "default"> para ol // Foto de eberhard grossgasteiger no Pexels-->
						<li> <a href ="areaPrivada.php"> Atualizar </a> </li>
						
						<li> <a href ="compra.php"> Compra </a> </li> 
					
						<li> <a href ="venda.php"> Venda </a> </li>
						
						<li> <a href ="pesagem.php"> Pesagem </a> </li>
						
						<li>  Gastos 
							<ul id="sub">
								<li id="prima"> <a href ="gastoIndividual.php"> Individual </a> </li>
								<li> <a href ="gastoRebanho.php"> Rebanho </a> </li>
							</ul>
						</li>
						
						<li> <a href ="nascidos.php"> Nascidos </a> </li>
						
						<li> <a href ="mortes.php"> Mortes </a> </li>
						
						<li> <a href ="relatorios.php"> Relatorios </a> </li>
						
						<li id="sair"> <a href ="classes/sair.php"> Sair </a> </li>
					</ul>
				</nav>
				
				<img id="icone" src = "_imagens/logo2.png"/>
				
			</header>
		</div>
		
		
		
		<img id="inicio" src = "_imagens/ap9.png" width="1333"/>
		
<!--============================================================================================================================-->
		
		
		<div id="desenvolvimento"> 
			
			<section>
				
				<?php
					$dadosUsuario;
					$dadosFazenda;
					
					if(dadosUsuario()){
						if(dadosFazenda()){
							if(dadosrebanho()){
								contarAnimais();
								contarAnimaisMachos();
								contarAnimaisFemeas();
							}
						}
					}
/*===================================================================================================================================*/
					
					if($dadosUsuario!=null && $dadosFazenda!=null){
						echo " <p> <h1> Fazenda $dadosFazenda[2] </h1> 
							   <h2> $dadosFazenda[3] </h2>";
							
						echo " <h2> Proprietario $dadosUsuario[1] </h2> </p>";
						
						echo "<h2> Rebanho Contabilizado em ".$numeroAnimais[0]." Animais! </h2>";
						
						echo "<h2> Em um Total de ".$numeroAnimaisFemeas[0]." Femaeas e ".$numeroAnimaisMachos[0]." Machos! </ h2>";
						
					}
/*===================================================================================================================================*/
					
				?>
				
			</section>
			
		</div>
		
		
	</body>
	
	
</html>