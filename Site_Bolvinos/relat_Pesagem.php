<?php
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_fazenda']) || !isset($_SESSION['id_rebanho']) ){
		header("location: login.php");
		exit;
	}
	
	include_once("classes/conexao.php");
/*--------------------------------------------------*/

/*===================================================================================================================================*/

	function buscardados($IntervaloDeTempo){
		
		$dataInicio = date('Y-m-d', strtotime('-'.$IntervaloDeTempo.' days', strtotime(date('Y-m-d'))));
		
		$dataCliente = date('d/m/Y', strtotime($dataInicio));
		
		echo "<p> Serão Contabilizadas Todas as Compras que Foram Realizadas </p> <p id='a'> Entre ".$dataCliente." e ".date('d/m/Y')."</p>";
		if(contarAnimais($dataInicio)){
			if(contarAnimaisMachos($dataInicio)){
				if(contarAnimaisFemeas($dataInicio)){
					valorCompra($dataInicio);
				}
			}
		}
	}
	
/*===================================================================================================================================*/
?>


<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Menu do Usuario </title>
		<link rel="stylesheet" href="_css/r_Compra.css">
		
		<script>
			function imprimir(){
				var elem = document.getElementById("imprimir");
				
				elem.style.visibility = "hidden"; 
				
				window.print();
				
				elem.style.visibility = "visible";
			
			}
		</script>
		
	</header>
	
	
	<body>
		<h1> Relatorio de Pesagem </h1>
		
		
		<fieldset id='maior'> <legend> &nbsp; Pesagens Realizadas &nbsp;</legend>
			
		</fieldset>
		
		
		<button onclick="imprimir()" id="imprimir"> Imprimir Relatorio </button>
	
	</body>
	
</html>