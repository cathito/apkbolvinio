<?php
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_fazenda']) || !isset($_SESSION['id_rebanho']) ){
		header("location: login.php");
		exit;
	}
	
	include_once("classes/conexao.php");
/*--------------------------------------------------*/

	$numeroAnimais;
	$numeroAnimaisMachos;
	$numeroAnimaisFemeas;
	$valorDaCompra;
	
/*===================================================================================================================================*/
	
	function contarAnimais($dataInicio){
		global $conn;
		global $numeroAnimais;
		
		$sql = "SELECT count(*) FROM animal where id_rebanho = ".$_SESSION['id_rebanho']." and emPosse = true and dataChegada >= '".$dataInicio."';";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			$numeroAnimais = mysqli_fetch_array($resultado);
			return true;
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01');
				window.history.back();
			</script>";
			return false;
		}
	}
	
/*===================================================================================================================================*/
	function contarAnimaisMachos($dataInicio){
			global $conn;
			global $numeroAnimaisMachos;
			
			$sql = "select count(*) from animal where sexo='M' and id_rebanho = ".$_SESSION['id_rebanho']." and emPosse = true and dataChegada >= '".$dataInicio."';";
			$resultado = mysqli_query($conn, $sql);
			
			if ($resultado) {
				$numeroAnimaisMachos = mysqli_fetch_array($resultado);
				return true;
			}else {
				echo $sql."<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 02');
					window.history.back();
				</script>";
				return false;
			}
		}
/*===================================================================================================================================*/
		function contarAnimaisFemeas($dataInicio){
			global $conn;
			global $numeroAnimaisFemeas;
			
			$sql = "select count(*) from animal where sexo='F' and id_rebanho = ".$_SESSION['id_rebanho']." and emPosse = true and dataChegada >= '".$dataInicio."';";
			$resultado = mysqli_query($conn, $sql);
			
			if ($resultado) {
				$numeroAnimaisFemeas = mysqli_fetch_array($resultado);
				return true;
			}else {
				echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 03');
					window.history.back();
				</script>";
				return false;
			}
		}
		
/*===================================================================================================================================*/

	function valorCompra($dataInicio){
			global $conn;
			global $valorDaCompra;
			$valorDaCompra=0;
			
			$sql = "select sum(valorAnimal) from dadosCompra where id_rebanho = ".$_SESSION['id_rebanho']." and emPosse = true and dataChegada >= '".$dataInicio."';";
			$resultado = mysqli_query($conn, $sql);
			
			if ($resultado) {
				$valorDaCompra = mysqli_fetch_array($resultado);
				return true;
			}else {
				echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 03');
					window.history.back();
				</script>";
				return false;
			}
		}
		
/*===================================================================================================================================*/
	function buscardados($IntervaloDeTempo){
		
		$dataInicio = date('Y-m-d', strtotime('-'.$IntervaloDeTempo.' days', strtotime(date('Y-m-d'))));
		
		$dataCliente = date('d/m/Y', strtotime($dataInicio));
		
		echo "<p> Serão Contabilizadas Todas as Compras que Foram Realizadas </p> <p id='a'> Entre ".$dataCliente." e ".date('d/m/Y')."</p>";
		if(contarAnimais($dataInicio)){
			if(contarAnimaisMachos($dataInicio)){
				if(contarAnimaisFemeas($dataInicio)){
					valorCompra($dataInicio);
				}
			}
		}
	}
	
/*===================================================================================================================================*/
?>


<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Menu do Usuario </title>
		<link rel="stylesheet" href="_css/r_Compra.css">
		
		<script>
			function imprimir(){
				var elem = document.getElementById("imprimir");
				
				elem.style.visibility = "hidden"; 
				
				window.print();
				
				elem.style.visibility = "visible";
			
			}
		</script>
		
	</header>
	
	
	<body>
		
		<section>
			<h1> Relatorio de Compra </h1>
			
			<fieldset id='maior'> <legend> &nbsp; Compras Realizadas na Ultima Semana &nbsp;</legend>
				<?php
					buscardados(7);
					global $numeroAnimais;
					global $numeroAnimaisMachos;
					global $numeroAnimaisFemeas;
					global $valorDaCompra;
					
					echo"<table id='tabelaspec'>
					
						<tr> <td class='ce'> Foram Comprados: </td> <td class='cd'> ".$numeroAnimais[0]." Animais </td></tr>
						
						<tr> <td rowspan='2' class='ce'> Em um Total de: </td> <td class='cd'> ".$numeroAnimaisFemeas[0]." Femeas</td> </tr>
																		  <tr> <td class='cd'> ".$numeroAnimaisMachos[0]." Machos </td> </tr>
						
						<tr> <td rowspan='2' class='ce'> Tendo Um Valor Estimado Em: </td> <td class='cd'> R$ ".$valorDaCompra[0]." reais</td> </tr>
						
					</table>";
				?>
			</fieldset>
			
			
			
			<fieldset id='maior'> <legend> &nbsp; Compras Realizadas no Ultimo Mês &nbsp;</legend>
			
				<?php
					buscardados(30);
					global $numeroAnimais;
					global $numeroAnimaisMachos;
					global $numeroAnimaisFemeas;
					global $valorDaCompra;
					
					echo"<table id='tabelaspec'>
					
						<tr> <td class='ce'> Foram Comprados: </td> <td class='cd'> ".$numeroAnimais[0]." Animais </td></tr>
						
						<tr> <td rowspan='2' class='ce'> Em um Total de: </td> <td class='cd'> ".$numeroAnimaisFemeas[0]." Femeas</td> </tr>
																		  <tr> <td class='cd'> ".$numeroAnimaisMachos[0]." Machos </td> </tr>
						
						<tr> <td rowspan='2' class='ce'> Tendo Um Valor Estimado Em: </td> <td class='cd'> R$ ".$valorDaCompra[0]." reais</td> </tr>
						
					</table>";
				?>
				
			</fieldset>
			
			
			
			<fieldset id='maior'> <legend> &nbsp; Compras Realizadas no Ultimo Trimeste &nbsp;</legend>
				
				<?php
					buscardados(91);
					global $numeroAnimais;
					global $numeroAnimaisMachos;
					global $numeroAnimaisFemeas;
					global $valorDaCompra;
					
					echo"<table id='tabelaspec'>
					
						<tr> <td class='ce'> Foram Comprados: </td> <td class='cd'> ".$numeroAnimais[0]." Animais </td></tr>
						
						<tr> <td rowspan='2' class='ce'> Em um Total de: </td> <td class='cd'> ".$numeroAnimaisFemeas[0]." Femeas</td> </tr>
																		  <tr> <td class='cd'> ".$numeroAnimaisMachos[0]." Machos </td> </tr>
						
						<tr> <td rowspan='2' class='ce'> Tendo Um Valor Estimado Em: </td> <td class='cd'> R$ ".$valorDaCompra[0]." reais</td> </tr>
						
					</table>";
				?>
				
			</fieldset>
			
			
			
			<fieldset id='maior'> <legend> &nbsp; Compras Realizadas no Ultimo Semestre &nbsp;</legend>
				
				<?php
					buscardados(182);
					global $numeroAnimais;
					global $numeroAnimaisMachos;
					global $numeroAnimaisFemeas;
					global $valorDaCompra;
					
					echo"<table id='tabelaspec'>
					
						<tr> <td class='ce'> Foram Comprados: </td> <td class='cd'> ".$numeroAnimais[0]." Animais </td></tr>
						
						<tr> <td rowspan='2' class='ce'> Em um Total de: </td> <td class='cd'> ".$numeroAnimaisFemeas[0]." Femeas</td> </tr>
																		  <tr> <td class='cd'> ".$numeroAnimaisMachos[0]." Machos </td> </tr>
						
						<tr> <td rowspan='2' class='ce'> Tendo Um Valor Estimado Em: </td> <td class='cd'> R$ ".$valorDaCompra[0]." reais</td> </tr>
						
					</table>";
				?>
				
			</fieldset>
			
			
			
			<fieldset id='maior'> <legend> &nbsp; Compras Realizadas no Ultimo Ano &nbsp;</legend>
				
				<?php
					buscardados(182);
					global $numeroAnimais;
					global $numeroAnimaisMachos;
					global $numeroAnimaisFemeas;
					global $valorDaCompra;
					
					echo"<table id='tabelaspec'>
					
						<tr> <td class='ce'> Foram Comprados: </td> <td class='cd'> ".$numeroAnimais[0]." Animais </td></tr>
						
						<tr> <td rowspan='2' class='ce'> Em um Total de: </td> <td class='cd'> ".$numeroAnimaisFemeas[0]." Femeas</td> </tr>
																		  <tr> <td class='cd'> ".$numeroAnimaisMachos[0]." Machos </td> </tr>
						
						<tr> <td rowspan='2' class='ce'> Tendo Um Valor Estimado Em: </td> <td class='cd'> R$ ".$valorDaCompra[0]." reais</td> </tr>
						
					</table>";
				?>
				
			</fieldset>
			<br/>
			
			<button onclick="imprimir()" id="imprimir"> Imprimir Relatorio </button>
			
			<br/>
		</section>
		
		
	</body>
	
</html>