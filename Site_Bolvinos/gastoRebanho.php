<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Menu do Usuario </title>
		<link rel="stylesheet" href="_css/compra.css">
		<link rel="stylesheet" href="_css/cabecalhoPrivadoPosInicial.css">
	</header>
	
	
	<body>
		
		<div id="cabecalho"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<ul type = "default" ><!-- start = "default"> para ol // Foto de eberhard grossgasteiger no Pexels-->
						<li> <a href ="areaPrivada.php"> Tela Inicial </a> </li>
						
						<li> <a href ="compra.php"> Compra </a> </li> 
					
						<li> <a href ="venda.php"> Venda </a> </li>
						
						<li> <a href ="pesagem.php"> Pesagem </a> </li>
						
						<li>  Gastos 
							<ul id="sub">
								<li id="prima"> <a href ="gastoIndividual.php"> Individual </a> </li>
								<li> <a href ="gastoRebanho.php"> Rebanho </a> </li>
							</ul>
						</li>
						
						<li> <a href ="nascidos.php"> Nascidos </a> </li>
						
						<li> <a href ="mortes.php"> Mortes </a> </li>
						
						<li> <a href ="relatorios.php"> Relatorios </a> </li>
						
					</ul>
				</nav>
				
				<img id="icone" src = "_imagens/logo2.png"/>
				
			</header>
		</div>
		
		
		<img id="inicio" src = "_imagens/ap9.png" width="1333"/>
		
<!--============================================================================================================================-->
		
		
		<div id="desenvolvimento"> 
			
			<form method="POST" action="classes/cadGastoRebanho.php">
				<h1> Cadastar Gasto Com o Rebanho</h1>
				<section id ="dois">
					<p id="primeiro"> Valor Gasto: R$ <input type="text" placeholder="Valor Gasto" name="valorGasto" maxlength="7"/> </p>
					
					<p> Data <input type="date" name="data" /> </p>
				</section>
				
				<section>
					<p> Causa do Gasto: <textarea name="causagasto" cols="70" rows="3" placeholder="Causa Gasto..."></textarea>  </p>
				</section>
				
				<input type="submit" value="Cadastrar" id="cadastrar" name="cadastrar">
				
			</form>
			
		</div>
		
		
	</body>
	
	
</html>