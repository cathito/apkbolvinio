<!DOCTYPE html>

<html lang = "pt-br">

	<header>
		<meta charset="utf-8"/>
		<title> Projeto Login </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="stylesheet" href="_css/t_login.css"/>
	</header>
	
<!--=======================================================================================================================================-->
	
	<body>
	
		<header id = "cabecalho">
			<nav id = "menu">
				<ul type = "default" ><!-- start = "default"> para ol-->
					<li> <a href ="	index.php"> Página Inicial </a> </li>
					
					<li> <a href ="login.php"> Login </a> </li>
					
					<li> <a href ="faleConosco.php"> Fale conosco </a> </li>
				</ul>
			</nav>
			
			<img id="icone" src = "_imagens/logo2.png"/>
			
		</header>
		
	
<!--=======================================================================================================================================-->

		
		
		<img id="inicio" src = "_imagens/t2.png" width="1333" />
		
		
		
		<div id="corpo-form">
			<form method="POST" action="classes/login.php">
			
				<h1> Entrar </h1>
				<input type="text" placeholder="Login" name="login" maxlength="12"/>
				<input type="password" placeholder="Senha" name="senha" maxlength="12"/>
				<input type="submit" value="Entrar" id="entrar" name="entrar">
				
				<p> Ainda Não é Inscrito? <a href="cadastrar.php"> <strong> Cadastre-Se </strong> </a> </p>
				<p> <a href="esqueciSenha.php" name="b"> <strong> Esqueceu a senha? </strong> </a> </p> 
				
			</form>
			<br/>
		</div>
		
		
		
<!--=======================================================================================================================================-->

	</body>
	
	
</html>