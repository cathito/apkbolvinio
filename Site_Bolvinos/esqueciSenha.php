<!DOCTYPE html>

<html lang = "pt-br">

	<header>
		<meta charset ="utf-8">
		<title> Projeto Login </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel = "stylesheet" href = "_css/esqueciSenha.css"/>
	</header>
	
<!--=======================================================================================================================================-->
	
	<body>
	
		<header id = "cabecalho">
			<nav id = "menu">
				<ul type = "default" ><!-- start = "default"> para ol-->
					<li> <a href ="	index.php"> Página Inicial </a> </li>
					
					<li> <a href ="login.php"> Login </a> </li>
					
					<li> <a href ="escolhaSimulado.php"> Simulados </a> </li>
					
					<li> <a href ="faleConosco.php"> Fale conosco </a> </li>
				</ul>
			</nav>
			
			<img id="icone" src = "_imagens/logo2.png"/>
			
		</header>
		
<!--=======================================================================================================================================-->

		<div id="corpo-form">
		
			<form method="POST" >
				<h1> Esqueci a Senha </h1>
				<input type="text" placeholder="Email" name="email" maxlength="50"/>
				
				<input type="submit" name="a" onclick="t1()" value="Recuperar Senha!"/>
				
			</form>
			
			<p id="demo"> Aplicação em Desenvolvimento! </p>
			
		</div>
		
<!--=======================================================================================================================================-->

		<div id="rodape">
			
			<p id="campos">
				<a href="termosDeServicos.php" id="i"> Termos de Serviço </a> //
					
				<a href="referenciasBibliográfica.php" id="i"> Referencias Bibliográfica </a>
			</p>
				
			<footer>
				
				<p> Copyright &copy; 2020 - by João Ferreira <br/>
				<a href="https://www.facebook.com/profile.php?id=100013593917417" target="_blank" id="facebook"> Facebook</a> |
				
				<a href="https://www.instagram.com/joao.ferreira9899" target="_blank" id="instagram"> Instagram </a>
				</p>
				
			</footer>
			
		</div>
	
<!--=======================================================================================================================================-->	
	
	
	</body>
	
	
</html>