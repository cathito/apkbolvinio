drop database apkbolvino;

create database apkbolvino
default character set utf8
default collate utf8_general_ci;

use apkbolvino;

create TABLE loginn ( 
	id_login int not null auto_increment, 
	login VARCHAR(12) NOT NULL, 
	senha VARCHAR(12) NOT NULL,
    primary key(id_login) )
    default charset=utf8;

create table usuario( 
     id_usuario    int not null auto_increment,
	 id_login    int   not null,
     nome    varchar(52)  not null,
	 email    varchar(40)  not null,
     primary key( id_usuario ) ) 
     default charset=utf8; 
     
create table fazenda( 
     id_fazenda    int not null auto_increment,
	 id_login    int   not null,
     nome    varchar(52)  not null,
	 endereco    varchar(52)  not null,
     primary key( id_fazenda ) ) 
     default charset=utf8; 
     
/*===================================================================*/


create table rebanho( 
     id_rebanho    int not null auto_increment,
	 id_fazenda    int   not null,
     primary key( id_rebanho ) ) 
     default charset=utf8; 
     
create table animal( 
     id_animal   int not null auto_increment,
	 id_rebanho    int   not null,
     numeroDeIdentificacao int not null,
     sexo  enum('M','F') not null,
     raca varchar(20) not null,
	 dataChegada date not null,
     chegadaPorCompra boolean default true,
     emPosse boolean default true,
     primary key( id_animal ) ) 
     default charset=utf8; 


/*===================================================================*/


create table pesagem( 
     id_pesagem   int not null auto_increment,
     id_rebanho    int   not null,
     id_animal  int   not null,
     pesoA decimal(5,2) not null,
     pesokg decimal(7,2) not null,
     dataPesagem date not null,
     primary key( id_pesagem ) ) 
     default charset=utf8; 


create table gastoIndividual( 
     id_gastoIndividual   int not null auto_increment,
     id_rebanho    int   not null,
     id_animal  int   not null,
     valorGasto  decimal(9,2) not null,
     dataGasto date not null,
     causaGasto text,
     primary key( id_gastoIndividual ) ) 
     default charset=utf8; 
	
create table gastoRebanho( 
     id_gastoRebanho   int not null auto_increment,
     id_rebanho    int   not null,
     valorGasto  decimal(9,2) not null,
     dataGasto date not null,
     causaGasto text,
     primary key( id_gastoRebanho ) ) 
     default charset=utf8; 
     
/*===================================================================*/


create table dadosCompra( 
     id_dadosCompra   int not null auto_increment,
     id_rebanho    int   not null,
     id_animal  int   not null,
	 pesoA decimal(5,2) not null,
     pesokg decimal(7,2) not null,
     valorA decimal(9,2) not null,
     valorAnimal decimal(9,2) not null,
     dataChegada date not null,
     emPosse boolean default true,
     primary key(Id_dadosCompra ) ) 
     default charset=utf8; 

 create table saidaMorte( 
     id_saida   int not null auto_increment,
     id_rebanho    int   not null,
     id_animal  int   not null,
     dataSaida date not null,
     causaMorte text,
     primary key( id_saida ) ) 
     default charset=utf8; 
     
     
create table saidaVenda( 
     id_saidaVenda   int not null auto_increment,
     id_rebanho    int   not null,
     id_animal  int   not null,
	 pesoA decimal(5,2) not null,
     pesokg decimal(7,2) not null,
     valorA int not null,
     valorAnimal decimal(9,2) not null,
     dataSaida date not null,
     primary key( id_saidaVenda ) ) 
     default charset=utf8; 