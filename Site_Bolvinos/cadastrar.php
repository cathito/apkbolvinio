<!DOCTYPE html>

<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Projeto Login </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="stylesheet" href="_css/t_login.css"/>
	</header>
	
	
	<body id="cadastro">
	
		<header id = "cabecalho">
			<nav id = "menu">
				<ul type = "default" ><!-- start = "default"> para ol-->
					<li> <a href ="	index.php"> Página Inicial </a> </li>
					
					<li> <a href ="login.php"> Login </a> </li>
					
					<li> <a href ="faleConosco.php"> Fale conosco </a> </li>
				</ul>
			</nav>
			
			<img id="icone" src = "_imagens/logo2.png"/>
			
		</header>
		
	
<!--=======================================================================================================================================-->
		
		<img id="inicio" src = "_imagens/ponte.png" width="1333" />
		
		
		<div id="corpo-form">
			<form method="POST" action="classes/cadastro.php">
			
				<h1> Cadastre-Se </h1>
				
				<input type="text" placeholder="Nome Completo" name="nomeUsuario" maxlength="40"/>
				<input type="email" placeholder="Email" name="email" maxlength="40"/>
				
				<input type="text" placeholder="Nome Fazenda" name="nomeFazenda" maxlength="40"/>
				<input type="text" placeholder="Cidade" name="cidade" maxlength="20"/>
				<input type="text" placeholder="Estado" name="estado" maxlength="2"/>
				
				<input type="text" placeholder="Login" name="login" maxlength="12"/>
				<input type="password" placeholder="Senha" name="senha" maxlength="12"/>
				
				<input type="submit" value="Cadastrar" id="cadastrar" name="cadastrar">
				
			</form>
		</div>
		
		
	</body>
	
	
</html>