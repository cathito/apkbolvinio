<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Menu do Usuario </title>
		<link rel="stylesheet" href="_css/cabecalhoPrivadoPosInicial.css">
		<link rel="stylesheet" href="_css/relatorios.css">
		
	</header>
	
	
	<body>
		
		<div id="cabecalho"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<ul type = "default" ><!-- Foto de eberhard grossgasteiger no Pexels-->
						<li> <a href ="areaPrivada.php"> Tela Inicial </a> </li>
						
						<li> <a href ="compra.php"> Compra </a> </li> 
					
						<li> <a href ="relat_Venda.php"> Venda </a> </li>
						
						<li> <a href ="pesagem.php"> Pesagem </a> </li>
						
						<li>  Gastos 
							<ul id="sub">
								<li id="prima"> <a href ="gastoIndividual.php"> Individual </a> </li>
								<li> <a href ="gastoRebanho.php"> Rebanho </a> </li>
							</ul>
						</li>
						
						<li> <a href ="nascidos.php"> Nascidos </a> </li>
						
						<li> <a href ="mortes.php"> Mortes </a> </li>
						
						
					</ul>
				</nav>
				
				<img id="icone" src = "_imagens/logo2.png"/>
				
			</header>
		</div>
		
		
		<img id="inicio" src = "_imagens/ap9.png" width="1333"/>
		
<!--============================================================================================================================-->
		
		
		<div id="desenvolvimento"> 
			<h1> Relatorios </h1>
			
			
			<form method="POST" action="relat_Geral.php" >
				<input type="submit" value="Dados Gerais" name="dadosGerais"/>
			</form>
			
			
			<form method="POST" action="relat_Compra.php">
				<input type="submit" value="Compra" name="compra"/>
			</form>
		
			<form method="POST" action="relat_Venda.php" id="livre">
				<input type="submit" value="Venda" name="venda">
			</form>	
			
			
			<form method="POST" action="relat_Pesagem.php" id="livre">
				<input type="submit" value="Pesagem" name="pesagem"/>
			</form>	
			
			
			<form method="POST" action="relatorioGerais.php">
				<input type="submit" value="Nascidos" name="nascidos"/>
			</form>	
			
			
			<form method="POST" action="relatorioGerais.php" id="livre">
				<input type="submit" value="Mortes" name="mortes"/>
			</form>
			
			
			
			<form method="POST" action="relatorioGerais.php">
				<input type="submit" value="*****" name="mortes"/>
			</form>
			
			
		</div>
		
		
	</body>
	
	
</html>